Subtask = require('Subtask')

class GoTo extends Subtask
  constructor: ({superTask, targetPosition, targetObject, serializedData}) ->
    super({
      superTask: superTask
      className: "GoTo"
      serializedData: serializedData
    })

    if serializedData then return
    
    unless targetPosition?
      if not targetObject?
        console.log "Created GoTo subtask with no destination.
                     Creep: #{superTask.getOwner().name},
                     Supertask: #{superTask.className}"
        return
      targetPosition = targetObject.pos
      
    creep = @getSuperTask().getOwner()
    @path = Room.serializePath(creep.room.findPath(creep.pos, targetPosition, {
      ignoreCreeps: true
    }))

  isComplete: ->
    return true if @path.length == 0

    pathLastPos = @path[@path.length - 1]
    creep = @getSuperTask().getOwner()
    return creep.pos.x == pathLastPos.x and creep.pos.y == pathLastPos.y

  preTick: ->
    super()
    @path = Room.deserializePath(@path)

  tick: ->
    super()
    creep = @getSuperTask().getOwner()
    if ((err = creep.moveByPath(@path)) < OK)
      console.log "Move by path error: #{err} path: #{@path}"

  postTick: ->
    super()
    @path = Room.serializePath(@path)

module.exports = GoTo
