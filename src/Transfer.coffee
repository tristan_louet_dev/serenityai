Subtask = require 'Subtask'

class Transfer extends Subtask
  constructor: ({
    superTask
    targetObject
    @transferOnlyNb = -1
    serializedData
    })
  ->
    
    super({
      superTask: superTask
      className: "Transfer"
      serializedData: serializedData
    })

    if serializedData then return

    @alreadyTransferred = 0
