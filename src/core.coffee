# Serenity core logic

_ = require("lodash")

class MemoryObject
  @deserialize: (serializedData) ->
    Class = require(serializedData.className)
    return new Class(serializedData: serializedData)

  constructor: (serializedData, className) ->
    if serializedData?
      for key, value of serializedData
        this[key] = value
    else
      @className = className

class SerenityState extends MemoryObject
  constructor: (serializedData) ->
    super(serializedData, "SerenityState")

    unless serializedData?
      @nextAvailableId = 0
      @tasks = {}
  
  addTask: (task) ->
    taskId = @nextAvailableId
    @nextAvailableId++
    @tasks[taskId] = task
    return taskId
  
  removeTask: (task) ->
    delete @tasks[task.taskId]
  
  preTick: ->
    for taskId, taskData of @tasks
      if taskData?
        @tasks[taskId] = MemoryObject.deserialize(taskData)
        @tasks[taskId].preTick()
  
  tick: ->
    HarvestTask = require 'HarvestTask'
    for name, creep of Game.creeps
      unless creep.memory.hasTask == true or creep.spawning
        new HarvestTask(owner: creep)
    
    for taskId, task of @tasks
      if task?
        task.tick()
    
    return
  
  postTick: ->
    for taskId, task of @tasks
      task.postTick()

module.exports.MemoryObject = MemoryObject
module.exports.SerenityState = SerenityState
