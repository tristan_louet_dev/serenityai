GoTo = require 'GoTo'

class GoNear extends GoTo
  constructor: ({
    superTask
    targetPosition
    targetObject
    separatingSpace = 1
    serializedData
  }) ->
    super({
      superTask: superTask
      targetPosition: targetPosition
      targetObject: targetObject
      className: "GoNear"
      serializedData: serializedData
    })

    if serializedData then return

    @path = Room.deserializePath(@path)
    if @path.length <= separatingSpace
      @path = []
    else
      @path = _.dropRight(@path, separatingSpace)
    @path = Room.serializePath(@path)

module.exports = GoNear
