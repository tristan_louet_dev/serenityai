MemoryObject = require("core").MemoryObject
_ = require("lodash")

class Task extends MemoryObject
  constructor: ({owner, className, serializedData}) ->
    super(serializedData, className)
    unless serializedData? # default values
      if not owner?
        console.log "Tried to create a Task without a creep owner"
        return
      
      @ownerId = owner.id
      @taskId = Game.serenity.addTask(this)
      owner.memory.hasTask = true
      @subtasksQueue = []
  
  getOwner: ->
    return Game.getObjectById(@ownerId)
  
  addSubtask: (subtask) ->
    @subtasksQueue.push(subtask)
  
  preTick: ->
    if not @getOwner()?
      @giveUp()
      return

    # We only need to deserialize the first subtask
    if @subtasksQueue.length > 0
      @subtasksQueue[0] = MemoryObject.deserialize(@subtasksQueue[0])
      @subtasksQueue[0].preTick()

  tick: ->
    if @subtasksQueue.length > 0
      @subtasksQueue[0].tick()

  postTick: ->
    if @subtasksQueue.length > 0
      completed = @subtasksQueue[0].isComplete()
      @subtasksQueue[0].postTick()
      if completed
        @subtasksQueue[0] = null
        @subtasksQueue = _.compact(@subtasksQueue)
        
  
  giveUp: ->
    @getOwner().memory.hasTask = false if @getOwner()?
    Game.serenity.removeTask(this)

module.exports = Task
